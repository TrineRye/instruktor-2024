class Human {
    constructor() {
        this.posX = width / 2;
        this.posY = height / 2;
        this.speed = 3;
        this.width = 80;
        this.height = 115;
    }
    show() {
        image(front_standing, this.posX, this.posY, this.width, this.height)
    }

    moveLeft() {
        if (keyIsDown(LEFT_ARROW)) {
            this.posX -= this.speed;
        }
    }

    moveRight() {
        if (keyIsDown(RIGHT_ARROW)) {
            this.posX += this.speed;
        }
    }

    moveUp() {
        if (keyIsDown(UP_ARROW)) {
            image(back_walking, this.posX, this.posY, this.width, this.height)
            this.posY -= this.speed;
        }
    }

    moveDown() {
        if (keyIsDown(DOWN_ARROW)) {
            image(front_walking, this.posX, this.posY, this.width, this.height)
            this.posY += this.speed;
        }
    }
}
