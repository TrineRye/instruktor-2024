let character;
let front_standing;
let front_walking;
let back_walking;

function preload() {
    front_standing = loadImage("front_standing.png")
    front_walking = loadImage("front_walking.gif")
    back_walking = loadImage("back_walking.gif")
}

function setup() {
    createCanvas(windowWidth, windowHeight);
    character = new Human();
}

function draw() {
    background(220);

    if (keyIsPressed === false) {
        character.show();
    } else if (keyIsPressed === true && keyCode !== UP_ARROW &&
        keyCode !== DOWN_ARROW && keyCode !== LEFT_ARROW && keyCode !== RIGHT_ARROW) {
        character.show();
    } else {
        character.moveDown();
        character.moveLeft();
        character.moveRight();
        character.moveUp();
    }





}
